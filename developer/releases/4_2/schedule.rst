===========================
Qubes R4.2 release schedule
===========================


**Please note:** *This page is still an unfinished draft in progress. It is being updated as Qubes 4.2 development and testing continues.*

The table below is based on our :ref:`release schedule policy <developer/releases/version-scheme:release schedule>`.

.. list-table:: 
   :widths: 10 10 
   :align: center
   :header-rows: 1

   * - Date
     - Stage
   * - 2023-06-02
     - 4.2.0-rc1 release
   * - TODO
     - current-testing freeze before 4.2.0-rc2
   

